<?php
defined('BASEPATH') or exit('No Direct Script Access Allowed');

class HomeController extends CI_Controller {

  /**
   * function executed by default
   * @return View [description]
   */
  public function index() {
    $this->load->view('frontend/index.php');
  }
  /**
   * GET About Us Page
   * @return View [description]
   */
  public function getAboutUs() {
    $this->load->view('frontend/about-us.php');
  }
}