<?php include_once('includes/header.php'); ?>
<div class="wrapper">
    <div class="page-header clear-filter" filter-color="orange">
      <div class="page-header-image" data-parallax="true" style="background-image:url('<?php echo BASE_URL(); ?>public_html/assets/img/basic.jpg');">
      </div>
      <div class="container">
        <div class="content-center brand">
          <h1 class="h1-seo"><strong>Mega IT Park.</strong></h1>
          <h3>Learn, adapt, excel. Your pathway to career</h3>
          <button class="btn btn-primary btn-round" type="button">View Our Courses</button>
        </div>
        <h6 class="category category-absolute">Designed and
          <a href="http://invisionapp.com/" target="_blank">
          </a>Coded By - Sweksha Bhattarai
      </div>
    </div>
    <!--  -->
    <!--  End Modal -->
    <?php include_once('includes/footer.php'); ?>